<?php
$genders = array(0 => "Nam", 1 => "Nữ");
$faculties = array(
	"MAT" => "Khoa học máy tính",
	"KDL" => "Khoa học vật liệu"
);
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
	<link href='https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/ui-lightness/jquery-ui.css' rel='stylesheet'>
    <script src= "https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js" ></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" ></script>
    <title>Đăng ký tân sinh viên</title>
</head>
<body>
    <form method="POST" action="">
		<div class="error-message">
			<?php
				if ($_SERVER["REQUEST_METHOD"] == "POST") {
					if (!isset($_POST["username"]) || empty($_POST["username"])) {
						echo "<p style=\"color: red\">Hãy nhập tên</p>";
					}
					if (!isset($_POST["gender"])) {
						echo "<p style=\"color: red\">Hãy chọn giới tính</p>";
					}
					if (!isset($_POST["faculty"]) || empty($_POST["faculty"])) {
						echo "<p style=\"color: red\">Hãy chọn phân khoa</p>";
					}
					if (!isset($_POST["dob"]) || empty($_POST["dob"])) {
						echo "<p style=\"color: red\">Hãy nhập ngay sinh</p>";
					}
					else {
						if (preg_match("/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/", $_POST["dob"]) === 0) {
							echo "<p style=\"color: red\">Hãy nhập ngay sinh đúng định dạng</p>";
						}
					}
				}
			?>
		</div>
        <div class="input-field-div">
            <div class="label-div">
                <label for="name" class="required-input">Họ và tên</label>
            </div>
            <input class="text-input" type="text" id="username" name="username">
        </div>
        <div class="input-field-div">
            <div class="label-div">
                <label for="gender" class="required-input">Giới tính</label>
            </div>
			<div class="gender-div">
				<?php
					for ($i = 0; $i < count($genders); ++$i) {
						echo ("
							<div>
								<input type=\"radio\" name=\"gender\" value=\"$i\">
								<label>$genders[$i]</label>
							</div>
						");
					}
				?>
			</div>
        </div>
		<div class="input-field-div">
            <div class="label-div">
                <label for="faculty" class="required-input">Phân khoa</label>
            </div>
			<select name="faculty" id="faculty">
				<?php
					echo "<option></option>";
					foreach ($faculties as $faculty) {
						echo "<option value=\"$faculty\">$faculty</option>";
					}
				?>
			</select>
        </div>
		<div class="input-field-div">
			<div class="label-div">
				<label for="dob" class="required-input">Ngày sinh</label>
			</div>
			<input class="my-date-picker" type="text" id="dob" name="dob" placeholder="dd/mm/yyyy">
		</div>
		<div class="input-field-div">
			<div class="label-div">
				<label for="address">Địa chỉ</label>
			</div>
			<input class="text-input" type="text" id="address" name="address">
		</div>
        <div class="submit-div">
            <input type="submit" value="Đăng ký"></input>
        </div>
    </form>
	
	<script type="text/javascript">
		$(document).ready(function() {
			$(function() {
				$("#dob").datepicker({
					dateFormat: 'dd/mm/yy'
				});
			});
		})
	</script>
</body>
</html>
